package com.magnetico.interview;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.magnetico.interview.repository.ClienteRepository;



@RunWith(SpringRunner.class)
@DataJpaTest
public class ClienteRepositoryIntegrationTest {
	@Autowired
	private ClienteRepository clienteRepository;
	@Test
	public void whenFindByDni_thenReturnClient() {
	    assertEquals("probando", "admin", clienteRepository.findByDni("99999999").getApellido());
	}
}