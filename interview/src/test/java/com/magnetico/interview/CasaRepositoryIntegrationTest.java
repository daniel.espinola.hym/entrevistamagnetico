package com.magnetico.interview;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.magnetico.interview.repository.CasaRepository;



@RunWith(SpringRunner.class)
@DataJpaTest
public class CasaRepositoryIntegrationTest {
	@Autowired
	private CasaRepository casaRepository;
	@Test
	public void whenFindAll_thenReturnAll() {
		System.out.println(casaRepository.findAll().size());
	    assertEquals("probando", 4, casaRepository.findAll().size(), 0);
	}
}