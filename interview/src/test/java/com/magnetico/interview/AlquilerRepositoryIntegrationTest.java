package com.magnetico.interview;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.magnetico.interview.entity.Alquiler;
import com.magnetico.interview.entity.Casa;
import com.magnetico.interview.entity.Cliente;
import com.magnetico.interview.repository.AlquilerRepository;
import com.magnetico.interview.repository.CasaRepository;
import com.magnetico.interview.repository.ClienteRepository;



@RunWith(SpringRunner.class)
@DataJpaTest
public class AlquilerRepositoryIntegrationTest {
	@Autowired
	private AlquilerRepository alquilerRepository;
	@Autowired
	private CasaRepository casaRepository;
	@Autowired
	private ClienteRepository clienteRepository;
	@Test
	public void whenTheUserIsAClient_thenGetBenefits() {
		Alquiler alquiler = new Alquiler();
		alquiler.setFechaInicio("10/10/2018");
		alquiler.setFechaFin("12/10/2018");
		
		Cliente cliente = clienteRepository.findByDni("99999999");		
		alquiler.setCliente(cliente);
		
		Casa casa = casaRepository.findById(1);
		alquiler.setCasa(casa);
		
		Alquiler alquilerNuevo = Mockito.spy(alquilerRepository.save(alquiler));
		
		assertEquals(0.05,alquilerNuevo.getDescuentoCliente() ,0);
	}
}