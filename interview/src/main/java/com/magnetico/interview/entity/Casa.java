package com.magnetico.interview.entity;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "casa")
public class Casa {
	@Id
	@Column(name="id_casa")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String direccion;
	private int cantidadDeAmbientes;
	private float mtsCuadrados;
	private double precioPorNoche;
	private double precioMensual;
	
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "casa")
	private Collection<Alquiler> alquileres;

	public Casa() {

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Collection<Alquiler> getAlquileres() {
		return alquileres;
	}

	public void setAlquileres(Collection<Alquiler> alquileres) {
		this.alquileres = alquileres;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public int getCantidadDeAmbientes() {
		return cantidadDeAmbientes;
	}

	public void setCantidadDeAmbientes(int cantidadDeAmbientes) {
		this.cantidadDeAmbientes = cantidadDeAmbientes;
	}

	public float getMtsCuadrados() {
		return mtsCuadrados;
	}

	public void setMtsCuadrados(float mtsCuadrados) {
		this.mtsCuadrados = mtsCuadrados;
	}

	public double getPrecioPorNoche() {
		return precioPorNoche;
	}

	public void setPrecioPorNoche(double precioPorNoche) {
		this.precioPorNoche = precioPorNoche;
	}

	public double getPrecioMensual() {
		return precioMensual;
	}

	public void setPrecioMensual(double precioMensual) {
		this.precioMensual = precioMensual;
	}

}
