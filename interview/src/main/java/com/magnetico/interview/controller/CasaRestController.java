package com.magnetico.interview.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.magnetico.interview.service.interfaces.ICasaService;
import com.magnetico.interview.util.ResponseWrapper;

@RestController
@RequestMapping("/casa")
public class CasaRestController {
	@Autowired
	private ICasaService casaService;
	
	@GetMapping("/getCasas")
	public ResponseWrapper saludar() {
		ResponseWrapper responseWrapper;
		try {
			System.out.println("llegamos");
			responseWrapper = new ResponseWrapper(casaService.findAll());
		}catch (Exception ex) {
			responseWrapper = new ResponseWrapper(ex);
		}
		return responseWrapper;
	}
}
