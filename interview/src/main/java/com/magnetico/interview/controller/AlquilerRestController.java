package com.magnetico.interview.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.magnetico.interview.entity.Alquiler;
import com.magnetico.interview.service.interfaces.IAlquilerService;
import com.magnetico.interview.util.ResponseWrapper;

@RestController
@RequestMapping("/alquiler")
public class AlquilerRestController {
	@Autowired
	private IAlquilerService alquilerService;
	
	@GetMapping("/getAlquileres")
	public ResponseWrapper getAllAlquileres() {
		ResponseWrapper responseWrapper;
		try {
			responseWrapper = new ResponseWrapper(alquilerService.findAll());
		}catch (Exception ex) {
			responseWrapper = new ResponseWrapper(ex);
		}
		return responseWrapper;
	}
	
	@PostMapping
	public ResponseWrapper alquilar(@RequestBody Alquiler alquiler) {
		ResponseWrapper responseWrapper;
		try {
			responseWrapper = new ResponseWrapper(alquilerService.save(alquiler));
		}catch (Exception ex) {
			ex.printStackTrace();
			responseWrapper = new ResponseWrapper(ex);
		}
		return responseWrapper;
	}
}
