package com.magnetico.interview.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.magnetico.interview.entity.Casa;

@Repository
public interface CasaRepository extends JpaRepository<Casa, Long>{
	 public Casa findById(long id);
	 
	 public List<Casa> findAll();
}
