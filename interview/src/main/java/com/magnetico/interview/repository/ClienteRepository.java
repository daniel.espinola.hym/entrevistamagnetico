package com.magnetico.interview.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.magnetico.interview.entity.Cliente;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long>{


	 public Cliente findByDni(String dni);

	 @SuppressWarnings("unchecked")
	 public Cliente save(Cliente cliente);
}
