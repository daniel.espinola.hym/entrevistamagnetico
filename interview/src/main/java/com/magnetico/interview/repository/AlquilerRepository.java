package com.magnetico.interview.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.magnetico.interview.entity.Alquiler;

@Repository
public interface AlquilerRepository extends JpaRepository<Alquiler, Long>{
	 public void delete(Alquiler alquiler);

	 public List<Alquiler> findAll();

	 public Optional<Alquiler> findById(Long id);

	 @SuppressWarnings("unchecked")
	 public Alquiler save(Alquiler alquiler);
}
