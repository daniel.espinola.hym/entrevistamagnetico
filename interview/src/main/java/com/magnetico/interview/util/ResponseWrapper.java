package com.magnetico.interview.util;

import java.util.HashMap;

public class ResponseWrapper {
	private int status;
	private HashMap<String, Object> respuesta;

	public ResponseWrapper() {
		
	}
	
	public ResponseWrapper(Exception e) {
		this.status = 500;
		respuesta = new HashMap<String, Object>();
		respuesta.put("error", "error interno");
	}
	
	public ResponseWrapper(Object object) {
		this.status = 200;
		respuesta = new HashMap<String, Object>();
		respuesta.put("datos", object);
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public HashMap<String, Object> getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(HashMap<String, Object> respuesta) {
		this.respuesta = respuesta;
	}

}
