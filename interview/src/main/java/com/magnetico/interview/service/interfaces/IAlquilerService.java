package com.magnetico.interview.service.interfaces;

import java.util.List;

import javax.transaction.Transactional;

import com.magnetico.interview.entity.Alquiler;
@Transactional
public interface IAlquilerService {
	public abstract List<Alquiler> findAll();
	public abstract Alquiler findById(Long id);
	public abstract Alquiler save(Alquiler alquiler) throws Exception;
	public abstract void delete(Alquiler alquiler);
}
