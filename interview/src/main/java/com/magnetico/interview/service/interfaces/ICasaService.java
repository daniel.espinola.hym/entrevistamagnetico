package com.magnetico.interview.service.interfaces;

import java.util.List;

import javax.transaction.Transactional;

import com.magnetico.interview.entity.Casa;
@Transactional
public interface ICasaService {
	public abstract List<Casa> findAll();
}
