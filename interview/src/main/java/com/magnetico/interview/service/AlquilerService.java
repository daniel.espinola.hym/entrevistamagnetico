package com.magnetico.interview.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magnetico.interview.entity.Alquiler;
import com.magnetico.interview.entity.Cliente;
import com.magnetico.interview.repository.AlquilerRepository;
import com.magnetico.interview.repository.CasaRepository;
import com.magnetico.interview.repository.ClienteRepository;
import com.magnetico.interview.service.interfaces.IAlquilerService;

@Service
public class AlquilerService implements IAlquilerService {

	@Autowired
	private AlquilerRepository alquilerRepository;
	@Autowired
	private ClienteRepository clienteRepository;
	@Autowired
	private CasaRepository casaRepository;

	@Override
	public List<Alquiler> findAll() {
		return alquilerRepository.findAll();
	}

	@Override
	public Alquiler findById(Long id) {
		return alquilerRepository.findById(id).get();
	}

	@Override
	public Alquiler save(Alquiler alquiler) throws Exception {
		alquiler.setCasa(casaRepository.findById(alquiler.getCasa().getId()));
		
		Cliente cliente = clienteRepository.findByDni(alquiler.getCliente().getDni());
		if(cliente!=null) {
			alquiler.setCliente(cliente);
		}
				
		double precioTotal = this.calcularPrecioTotal(alquiler);
		double bonificacionDeDias = this.calcularBonificacionesDias(alquiler);
		double bonificacionDeCliente = this.calcularBonificacionCliente(alquiler);
		
		alquiler.setPrecioTotal(precioTotal);
		alquiler.setDescuentoPorDias(bonificacionDeDias);
		alquiler.setDescuentoCliente(bonificacionDeCliente);
		
		double precioNeto = precioTotal - (precioTotal * (bonificacionDeCliente+bonificacionDeDias));
		alquiler.setPrecioNeto(precioNeto);
		
		alquiler.setFechaTransaccion(new Date());
		
		return alquilerRepository.save(alquiler);
	}

	@Override
	public void delete(Alquiler alquiler) {
		alquilerRepository.delete(alquiler);
	}
	
	/**
	 * metodo que devuelve el precio total del alquiler
	 * 
	 * @param alquiler
	 * @return
	 * @throws ParseException
	 */
	private double calcularPrecioTotal(Alquiler alquiler) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Calendar desde = Calendar.getInstance();
		Calendar hasta = Calendar.getInstance();
		desde.setTime(simpleDateFormat.parse(alquiler.getFechaInicio()));
		hasta.setTime(simpleDateFormat.parse(alquiler.getFechaFin()));

		int dias, meses, años;
		// calculo la diferencia de dias entre las dos fechas
		// le resto 1 para tener en cuenta el día en que se alquila
		dias = (hasta.get(Calendar.DAY_OF_MONTH)) - ((desde.get(Calendar.DAY_OF_MONTH)) - 1);

		// si da negativo quiere decir que no se cumplio un mes, pero si justo ese dia
		// es el ultimo dia del mes
		// si se cumple
		// ej: 31 de enero y termina el 28 de febrero, eso se cuenta como 1 mes
		if (0 > dias && hasta.getActualMaximum(Calendar.DAY_OF_MONTH) != hasta.get(Calendar.DAY_OF_MONTH)) {
			//resto uno a los meses porque no se cumplio un mes
			meses = (hasta.get(Calendar.MONTH)) - (desde.get(Calendar.MONTH)) - 1;
			//hago los dias positivos
			dias = dias * -1;
		} else {
			meses = (hasta.get(Calendar.MONTH)) - (desde.get(Calendar.MONTH));
		}

		// si los dias totales son igual al maximo numero del maximo de dias de la fecha fin
		// se incrementan los meses
		//ej: 1/1/2018 al 31/1/2018
		if (dias == hasta.getActualMaximum(Calendar.DAY_OF_MONTH)) {
			meses += 1;
			dias = 0;
		}

		// si los meses son negativos resto uno al año para poner la verdadera
		// diferencia
		if (0 > meses) {
			años = (hasta.get(Calendar.YEAR)) - (desde.get(Calendar.YEAR)) - 1;
			// si ese caso pasa debo calcular el total de meses transcurridos
			meses += 12 - (hasta.get(Calendar.MONTH));
		} else {
			años = (hasta.get(Calendar.YEAR)) - (desde.get(Calendar.YEAR));
		}

		if (meses == 12) {
			años += 1;
			meses = 0;
		}

		

		double total = ((años * alquiler.getCasa().getPrecioMensual() * 12)
				+ (meses * alquiler.getCasa().getPrecioMensual()) + (dias * alquiler.getCasa().getPrecioPorNoche()));

		return total;

	}
	
	/**
	 * metodo que calcula la bonificacion del precio por dias
	 * @param diasTotales
	 * @param cliente
	 * @return
	 * @throws ParseException 
	 */
	private double calcularBonificacionesDias(Alquiler alquiler) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Calendar desde = Calendar.getInstance();
		Calendar hasta = Calendar.getInstance();
		desde.setTime(simpleDateFormat.parse(alquiler.getFechaInicio()));
		hasta.setTime(simpleDateFormat.parse(alquiler.getFechaFin()));
		
		int diasTotales = (int) ((hasta.getTimeInMillis() - desde.getTimeInMillis()) / (1000 * 60 * 60 * 24));
		//se suma un dia porque la fecha del hasta deberia contarla hasta las 24hs de ese dia
		//y lo esta contando desde las 0hs de ese dia
		diasTotales++;
		
		double descuentoDeDias = 0;
		if (diasTotales > 15) {
			descuentoDeDias = 0.15;
		} else if (diasTotales > 4) {
			descuentoDeDias = 0.05;
		}
		return descuentoDeDias;
	}
	
	/**
	 * metodo que calcula la bonificacion precio del cliente
	 * @param alquiler
	 * @return
	 */
	private double calcularBonificacionCliente(Alquiler alquiler) {
		if(alquiler.getCliente().getId()!=0) {
			return 0.05;
		}
		return 0;
	}

}
