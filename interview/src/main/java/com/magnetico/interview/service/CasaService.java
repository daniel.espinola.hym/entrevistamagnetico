package com.magnetico.interview.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magnetico.interview.entity.Casa;
import com.magnetico.interview.repository.CasaRepository;
import com.magnetico.interview.service.interfaces.ICasaService;

@Service
public class CasaService implements ICasaService {

	@Autowired
	private CasaRepository casaRepository;

	@Override
	public List<Casa> findAll() {
		return casaRepository.findAll();
	}

}
