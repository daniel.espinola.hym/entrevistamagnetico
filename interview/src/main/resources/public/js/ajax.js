ajaxService = {
	sendRequest: function(url, type, data, eventoSuccess, eventoError) {
		$("body").css("cursor", "progress");
		$.ajax({
			url : url,
			type : type,
			dataType : 'json',
			data : data,
			contentType:"application/json",
			success : function(respuesta, textStatus, jqXHR) {
				$("body").css("cursor", "default");
				eventoSuccess(respuesta);
			},
			error : function(jqXHR, textStatus, errorThrown) {
				$("body").css("cursor", "default");
				eventoError(jqXHR);
			}
		});
    }
}