$( document ).ready(function() {
	configuraciones.iniciar();
});

configuraciones = {
	iniciar : function(){
		configuraciones.ocultar();
		configuraciones.opcionesDefault();
	},
	
	opcionesDefault: function(){
		$(".navAlquileres").off('click')
			.on("click",function(){
				configuraciones.activarNav("navAlquileres");
				inmobiliaria.inicio();
			});
	},
	
	activarNav: function(nombre){
		$(".navOpcion").each(function(){$(this).removeClass("active")});
		$("."+nombre).addClass("active");
	},
	
	ocultar: function(){
		$("section").each(function(){$(this).addClass("oculto")});
	},
	
	mostrarModulo: function(nombre){
		$("section").each(function(){$(this).addClass("oculto")});
		$("#"+nombre).removeClass("oculto");
	},
	
	soloNumeros : function(event) {
	    var key;
	    if(window.event) { // IE
	    	key = event.keyCode;
	        if ((key < 48 || key > 57) && key != 46 && key != 45) {
	            window.event.keyCode = 0;
	            return false;
	        }
	    }
	    else if(event.which) { // Netscape/Firefox/Opera
	    	key = event.which;
	        if(key == 08) {
	        	return true;
	        }
	        if ((key < 48 || key >  57 || key == 8) && key != 46 && key != 45) {
	            return false;
	        }
	    }
	    return true;
	},
	
	toDate: function(date){
		if(!date){
			return "";
		}
		var fecha = date.split("-");
		return fecha[2]+"/"+fecha[1]+"/"+fecha[0];
	}
}