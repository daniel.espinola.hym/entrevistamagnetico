inmobiliaria = {
	
	nombreModulo: "htmlAlquileres",
	
	casas: {},
	
	inicio: function(){
		inmobiliaria.setScreenName();
		inmobiliaria.mostrarModulo(inmobiliaria.nombreModulo);
		inmobiliaria.getCasas();
	},
	
	setScreenName: function(){
		$("#screenName").html("<i class='fa fa-bar-chart'></i> Alquileres");
	},
	
	mostrarModulo: function(nombre){
		$("section").each(function(){$(this).addClass("oculto")});
		$("#"+nombre).removeClass("oculto");
	},
	
	asignarEventos: function(){
		$("#btnAlquilarCasa").off('click')
			.on("click",function(){
				inmobiliaria.alquilar();
		});
		
		$("#tipoAlquiler").off('change')
			.on("change", function(){
					inmobiliaria.mostrarAlquileres($(this).val());
				});
		
		$('.datepickerFechaDesde').datepicker({
			format: "dd/mm/yyyy",
			startView: 1,
			language: "es",
			autoclose: true
		});
		$('.datepickerFechaHasta').datepicker({
			format: "dd/mm/yyyy",
			startView: 1,
			language: "es",
			autoclose: true
		});
	},
	
	asignarEventosTablaAlquileres: function(){
		$("#tablaAlquileresBody td").off('click')
			.on("click", function(){
					$(this).closest("tr").addClass('seleccionado');
					$("#divOpcionesAlquiler").removeClass("oculto");
				});
	},
	
	mostrarAlquileres: function(value){
		var index = parseInt(value)-1;
		var casa = inmobiliaria.casas[index];
		var html="";
		if(index<0){
			html+="<tr>";
			html+="<td colspan=6><center>Debe ingresar una casa</center></td>";
			html+="</tr>";
		}else{
			html+="<tr>";
			html+="<td>"+casa.id+"</td><td>"+casa.direccion+"</td><td>"+casa.cantidadDeAmbientes+"</td><td>"+casa.mtsCuadrados+"</td><td>"+casa.precioPorNoche+"</td><td>"+casa.precioMensual+"</td>";
			html+="</tr>";
		}
		
		
		$("#tablaAlquileresBody").html(html);
		inmobiliaria.asignarEventosTablaAlquileres();
	},
	
	alquilar: function(){
		var dni = $("#txtDniUsuario").val();
		var fechaInicio = $("#txtFechaDesdeAlquiler").val();
		var fechaFin = $("#txtFechaHastaAlquiler").val();
		var casaId = $($("#tablaAlquileresBody tr.seleccionado td")[0]).html();
		
		var alquiler = {'fechaInicio':fechaInicio,'fechaFin':fechaFin, 'casa':{'id':casaId}, 'cliente':{'dni':dni}};
		console.log(alquiler);
		ajaxService.sendRequest('/alquiler','POST',JSON.stringify(alquiler),inmobiliaria.alquilarSuccess,inmobiliaria.alquilarError);
	},
	
	alquilarSuccess: function(jqXHR){
		var informacion = jqXHR.respuesta.datos;
		var descuentoTotal = (informacion.descuentoPorDias+informacion.descuentoCliente)*100;
		var html = "Transaccion realizada. Precio total: $"+informacion.precioTotal+", precio neto a pagar: $"+informacion.precioNeto+". Le fue otorgado un descuento del "+descuentoTotal+"%";
		alert(html);
		location.reload();
	},
	
	alquilarError: function(jqXHR, textStatus, errorThrown){
		alert("Se produjo un error al procesar la transacción, intente nuevamente.");
	},
	
	getCasas: function(){
		ajaxService.sendRequest('/casa/getCasas','GET','',inmobiliaria.getCasasSuccess,inmobiliaria.getCasasError);
	},
	
	getCasasSuccess: function(jqXHR){
		inmobiliaria.casas=jqXHR.respuesta.datos;
		var html="<option value=0>Seleccione...</option>";
		for(var x=1; x<=inmobiliaria.casas.length; x++){
			var index = x-1;
			html+="<option value="+x+">"+inmobiliaria.casas[index].direccion+"</option>";
		}
		$("#tipoAlquiler").html(html);
		inmobiliaria.asignarEventos();
	},
	
	getCasasError: function(jqXHR, textStatus, errorThrown){
		alert("Se produjo un error al procesar la transacción, intente nuevamente.");
	}
}