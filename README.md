# Magnetico inmobiliaria

## Sobre la tecnología

### Herramientas utilizadas

* Java 1.8
* SpringBoot
* JUnit 4
* JPA y Hibernate
* Base de datos H2 embebida
* Html
* Css
* JQuery

##Aclaraciones:
* Para calcular la cantidad de dias del alquiler se toman como referencia la inclusion de la fecha de inicio y la fecha de fin marcadas. Por ejemplo: si yo tomo el alquiler del 1/1/2018 hasta el 3/1/2018 estoy contando 3 dias.

* Si tomo el alquiler un dia 31/03/2018 y alquilo hasta el 30/04/2018 se contara como alquiler mensual, lo mismo con febrero (si alquilo el 31 de enero y termino en el ultimo dia de febrero)

* No tiene validaciones hechas y solo esta contemplado el camino success

